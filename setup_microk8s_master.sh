#!/bin/bash
# Autor: Ale Martinez - devops
# Date: 27/10/2023
# Version: 0.2

echo ""
echo "[MicroK8 installer for a Kubernetes cluster]"
while read -p "[1] Clean [2] Install [3] Renew Cert TLS [4] Close: " x
do
if [ ${x} = 2 ]; then
                echo ""
                echo "Setup: "
                sudo snap install microk8s --classic --channel=1.24/stable

                echo "Set permissions to MicroK8s..."
                sudo usermod -a -G microk8s $USER
                sudo chown -f -R $USER ~/.kube
                echo "[INFO]: Please run this command 'newgrp microk8s', after Install again."

                echo "Setup microk8s (without sudo) MasterNode..."
                microk8s.enable dns helm3 metrics-server rbac ingress host-access

                echo "Config microk8s..."
                sudo snap alias microk8s.kubectl kubectl
                sudo snap alias microk8s.helm3 helm
                microk8s.kubectl config view --raw > ~/.kube/config
                kubectl create ns qa-system
                kubectl config set-context $(kubectl config current-context) --namespace=qa-system
                helm repo add stable https://charts.helm.sh/stable
                echo "For add this node, copy: "
                microk8s add-node
                echo "Done."
                echo ""
                break
                exit 0;

elif [ ${x} = 1 ]; then
        echo ""
        echo "Clean microk8s..."
        #microk8s.reset
        sudo rm -rf /usr/local/bin/kubectl
        echo "For Delete all (warning clean data)..."
        sudo microk8s refresh-certs
        sudo snap remove microk8s
        echo "Done."
        echo ""
        break
        exit 0;

elif [ ${x} = 3 ]; then
       echo ""
       echo "Renew Cert TLS Kubernetes..."
       sudo microk8s refresh-certs -e ca.crt
       #microk8s config > ~/.kube/config
       echo "Done."
       echo ""
       break
       exit 0;

elif [ ${x} = 4 ]; then
       echo "Bye..."
       echo ""
       break
       exit 0;

fi
done